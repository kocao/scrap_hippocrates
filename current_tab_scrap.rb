require "selenium-webdriver"
require "chromedriver-helper"

fields_title = ["state","county","facility", "status","reason","startTime","expire_time"]
array_hospitals = []
options = Selenium::WebDriver::Chrome::Options.new
file = File.new('current_hospitals.json','w+')
options.add_argument('--headless')
driver = Selenium::WebDriver.for :chrome, options: options
url = "https://hippocrates.nj.gov/hospdivert/viewCurrentHospitalStatusEMSDivert.action?stateId=318&fromLogin=N"

driver.navigate.to url
driver.navigate.refresh

hospitals = driver.find_elements(:css, ".tabledata")

hospitals.each do |hospital|
  hash_hospital = {} 
  fields = hospital.find_elements(:css, "td")

  fields.each_with_index do |field, index|
    hash_hospital.merge!(fields_title[index] => field.text)      
  end
  array_hospitals << hash_hospital
end

file.write(array_hospitals.to_json)

driver.quit