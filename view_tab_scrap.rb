require "selenium-webdriver"
require "chromedriver-helper"

fields_title = ["facility", "status","reason","startTime","expire_time"]
array_hospitals = []
options = Selenium::WebDriver::Chrome::Options.new
file = File.new('view_hospitals.json','w+')
options.add_argument('--headless')
driver = Selenium::WebDriver.for :chrome, options: options
url = "https://hippocrates.nj.gov/hospdivert/viewAllHospitalStatusItemsEMSDivert.action?sortParam=county&ascOrDesc=asc&fromLogin=N"

driver.navigate.to url
driver.navigate.refresh
driver.execute_script("expandCollapseAll(1)")
counties = driver.find_elements(:css, ".viewAllHospitalTableBodyInner1")

counties.each do |county|
   county_name = county.find_element(:css, ".treeTitle>.tableTitle").text
   hospitals = county.find_elements(:css, ".viewAllHospitalTableBodyInner2>tbody>tr")
  
   hospitals.each do |hospital|
      hash_hospital = {"county" => county_name} 
      fields = hospital.find_elements(:css, "td")

      fields.each_with_index do |field, index|
        hash_hospital.merge!(fields_title[index] => field.text)      
      end
      array_hospitals << hash_hospital
   end
end

file.write(array_hospitals.to_json)

driver.quit